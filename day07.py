import unittest
from typing import Mapping, Optional, Dict

from utils import load_string, run_testCase, split_clean

TXT_TEST = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""


def parse_rules(txt: str) -> Mapping[str, Optional[Mapping[str, int]]]:
    txt = txt.replace(" bags", "")
    txt = txt.replace(" bag", "")
    txt = txt.replace(".", "")
    trules = split_clean(txt)
    rules: Dict[str, Optional[Mapping[str, int]]] = {}
    for trule in trules:
        tk, tv = trule.split(" contain ")
        if tv == "no other":
            rules[tk] = None
        else:
            rules[tk] = {c[2:]: int(c[0]) for c in tv.split(", ")}
    return rules


def part_1(txt: str) -> int:
    rules = parse_rules(txt)
    new_bags = ["shiny gold"]
    ok_bags = set(new_bags)
    while new_bags:
        found = set()
        for bag in new_bags:
            for k, v in rules.items():
                if v and bag in v.keys() and k not in ok_bags:
                    found.add(k)
        ok_bags = ok_bags | found
        new_bags = list(found)
    return len(ok_bags) - 1


def part_2(txt: str) -> int:
    rules = parse_rules(txt)
    nbags = 0
    new_bags = [("shiny gold", 1)]
    while new_bags:
        todo = []
        for bag, m in new_bags:
            if not rules[bag]:
                continue
            todo.extend([(k, v * m) for k, v in rules[bag].items()])
        nbags += sum([v for k, v in todo])
        new_bags = todo
    return nbags


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 4)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 32)
        self.assertEqual(part_2("""shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""), 126)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
