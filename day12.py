import unittest

from utils import load_string, run_testCase, split_clean

TXT_TEST = """F10
N3
F7
R90
F11"""


def part_1(txt: str) -> int:
    face: complex = 1
    p: complex = 0
    lines = split_clean(txt)
    for line in lines:
        action, value = line[0], int(line[1:])
        if action == "N":
            p += value * 1j
        elif action == "S":
            p -= value * 1j
        elif action == "E":
            p += value
        elif action == "W":
            p -= value
        elif action == "F":
            p += value * face
        elif action == "L":
            while value != 0:
                face = face * 1j
                value -= 90
        elif action == "R":
            while value != 0:
                face = face / 1j
                value -= 90
        else:
            raise ValueError("Unkown " + action)
    return int(abs(p.real) + abs(p.imag))


def part_2(txt: str) -> int:
    ship: complex = 0
    waypoint = 10 + 1j
    lines = split_clean(txt)
    for line in lines:
        action, value = line[0], int(line[1:])
        if action == "N":
            waypoint += value * 1j
        elif action == "S":
            waypoint -= value * 1j
        elif action == "E":
            waypoint += value
        elif action == "W":
            waypoint -= value
        elif action == "F":
            ship += waypoint * value
        elif action == "L":
            while value != 0:
                waypoint = waypoint * 1j
                value -= 90
        elif action == "R":
            while value != 0:
                waypoint = waypoint / 1j
                value -= 90
        else:
            raise ValueError("Unkown " + action)
    return int(abs(ship.real) + abs(ship.imag))


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 25)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 286)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
