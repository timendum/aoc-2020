import unittest

from utils import load_string, run_testCase, split_clean


def part_1(txt: str) -> int:
    highest = 0
    for line in split_clean(txt):
        row = sum([pow(2, i) if x == "B" else 0 for i, x in enumerate(line[6::-1])])
        column = sum([pow(2, i) if x == "R" else 0 for i, x in enumerate(line[:6:-1])])
        seat_id = row * 8 + column
        highest = max(highest, seat_id)
    return highest


def part_2(txt: str) -> int:
    ids = []
    for line in split_clean(txt):
        row = sum([pow(2, i) if x == "B" else 0 for i, x in enumerate(line[6::-1])])
        column = sum([pow(2, i) if x == "R" else 0 for i, x in enumerate(line[:6:-1])])
        ids.append(row * 8 + column)
    ids = sorted(ids)
    for i in range(1, len(ids) - 2):
        if ids[i - 1] + 1 != ids[i]:
            return ids[i] - 1
    return 0


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1("BFFFBBFRRR\nFFFBBBFRRR\nBBFFBBFRLL"), 820)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
