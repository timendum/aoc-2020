import inspect
from pathlib import Path
import unittest
from typing import List


def run_testCase(testcase):
    unittest.TextTestRunner().run(unittest.TestLoader().loadTestsFromTestCase(testcase))


def load_string(filename=None) -> str:
    if not filename:
        frame = inspect.stack()[1]
        module = inspect.getmodule(frame[0])
        fn = Path(module.__file__).absolute()
    with fn.with_name('data').joinpath(fn.name.replace(".py", ".txt")).open("rt") as ifile:
        return ifile.read().strip()


def split_clean(txt: str, splitter="\n") -> List[str]:
    arr = txt.split(splitter)
    return [e.strip() for e in arr]
