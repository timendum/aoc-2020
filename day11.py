import unittest
from collections import Counter
from typing import List

from utils import load_string, run_testCase, split_clean

TXT_TEST = """#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##"""

DELTAS = ((-1, 0), (1, 0), (0, 1), (0, -1), (-1, 1), (-1, -1), (1, 1), (1, -1))


class Ferry:
    def __init__(self, rows: List[List[str]]) -> None:
        self.rows = rows
        self._rrows = range(len(self.rows))
        self._rcols = range(len(self.rows[0]))

    def apply1(self) -> "Ferry":
        new_rows = [[c for c in r] for r in self.rows]
        for x in self._rrows:
            for y in self._rcols:
                if self.rows[x][y] == ".":
                    continue
                adjacents = ["x"] * 8
                for i, (dx, dy) in enumerate(DELTAS):
                    if x + dx not in self._rrows or y + dy not in self._rcols:
                        continue
                    adjacents[i] = self.rows[x + dx][y + dy]
                ca = Counter(adjacents)
                if ca["#"] >= 4:
                    new_rows[x][y] = "L"
                elif ca["#"] == 0:
                    new_rows[x][y] = "#"
        return Ferry(new_rows)

    def apply2(self) -> "Ferry":
        new_rows = [[c for c in r] for r in self.rows]
        for x in self._rrows:
            for y in self._rcols:
                if self.rows[x][y] == ".":
                    continue
                adjacents = ["x"] * 8
                for i, (dx, dy) in enumerate(DELTAS):
                    p = 1
                    while (
                        adjacents[i] not in ("L", "#")
                        and (x + (dx * p) in self._rrows)
                        and (y + (dy * p) in self._rcols)
                    ):
                        adjacents[i] = self.rows[x + dx * p][y + dy * p]
                        p += 1
                ca = Counter(adjacents)
                if ca["#"] >= 5:
                    new_rows[x][y] = "L"
                elif ca["#"] == 0:
                    new_rows[x][y] = "#"
        return Ferry(new_rows)

    @staticmethod
    def from_text(txt: str) -> "Ferry":
        rows = split_clean(txt)
        return Ferry([[c for c in r] for r in rows])

    def __eq__(self, other):
        if not isinstance(other, Ferry):
            return False
        for a, b in zip(self.rows, other.rows):
            for i, j in zip(a, b):
                if i != j:
                    return False
        return True

    def __str__(self) -> str:
        return "\n".join(["".join(row) for row in self.rows])

    @property
    def occupied(self) -> int:
        return sum([1 if c == "#" else 0 for row in self.rows for c in row])


def part_1(txt: str) -> int:
    ferry = Ferry.from_text(txt)
    new_ferry = ferry.apply1()
    while ferry != new_ferry:
        ferry = new_ferry
        new_ferry = ferry.apply1()
    return ferry.occupied


def part_2(txt: str) -> int:
    ferry = Ferry.from_text(txt)
    new_ferry = ferry.apply2()
    while ferry != new_ferry:
        print(new_ferry)
        ferry = new_ferry
        new_ferry = ferry.apply2()
    return ferry.occupied


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 37)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 26)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
