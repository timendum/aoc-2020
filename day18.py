import unittest
import re
from typing import List, Union, Callable


from utils import load_string, run_testCase, split_clean

_TOKENIZER = re.compile(r"(\+|\(|\*|\+|\)|\d+)")

SUM = lambda a, b: a + b
PRODUCT = lambda a, b: a * b


class Expression:
    def __init__(self, tokens: List[Union[str, "Expression"]]):
        while "(" in tokens:
            idx = tokens.index("(")
            inner = 0
            for i in range(idx + 1, len(tokens)):
                if tokens[i] == "(":
                    inner += 1
                if tokens[i] == ")":
                    if inner == 0:
                        tokens = tokens[:idx] + [Expression(tokens[idx + 1 : i])] + tokens[i + 1 :]
                        break
                    else:
                        inner -= 1
        self.terms: List[Union[str, Expression]] = tokens

    def __str__(self):
        return "Expression(" + " ".join([str(t) for t in self.terms]) + ")"

    @staticmethod
    def __loper(c) -> Callable[[int, int], int]:
        if c == "+":
            return SUM
        else:
            return PRODUCT

    def part_1(self) -> int:
        total = 0
        operation = self.__loper("+")
        for t in self.terms:
            if isinstance(t, Expression):
                num = t.part_1()
                total = operation(total, num)
            elif all([c in "0123456789" for c in t]):
                num = int(t)
                total = operation(total, num)
            else:
                operation = self.__loper(t)
        return total

    def part_2(self) -> int:
        total = 0
        operation = self.__loper("+")
        for i, t in enumerate(self.terms):
            if t == "*":
                self.terms = self.terms[: i + 1] + [Expression(self.terms[i + 1 :])]
                break
        for t in self.terms:
            if isinstance(t, Expression):
                num = t.part_2()
                total = operation(total, num)
            elif all([c in "0123456789" for c in t]):
                num = int(t)
                total = operation(total, num)
            else:
                operation = self.__loper(t)
        print(self, total)
        return total


def part_1(txt: str) -> int:
    lines = split_clean(txt)
    res = 0
    for line in lines:
        tokens = _TOKENIZER.findall(line)
        exp = Expression(tokens)
        p = exp.part_1()
        res += p
    return res


def part_2(txt: str) -> int:
    lines = split_clean(txt)
    res = 0
    for line in lines:
        tokens = _TOKENIZER.findall(line)
        exp = Expression(tokens)
        p = exp.part_2()
        res += p
    return res


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1("1 + 2 * 3 + 4 * 5 + 6"), 71)
        self.assertEqual(part_1("1 + (2 * 3) + (4 * (5 + 6)))"), 51)
        self.assertEqual(part_1("2 * 3 + (4 * 5)"), 26)
        self.assertEqual(part_1("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 437)
        self.assertEqual(part_1("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 12240)
        self.assertEqual(part_1("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"), 13632)

    def test_part_2(self):
        self.assertEqual(part_2("1 + 2 * 3 + 4 * 5 + 6"), 231)
        self.assertEqual(part_2("1 + (2 * 3) + (4 * (5 + 6)))"), 51)
        self.assertEqual(part_2("2 * 3 + (4 * 5)"), 46)
        self.assertEqual(part_2("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 1445)
        self.assertEqual(part_2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 669060)
        self.assertEqual(part_2("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"), 23340)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
