import unittest
import re

from utils import load_string, run_testCase, split_clean

TXT_TEST = """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""

TDATA = re.compile(r"(\w+):([^\s]+)")


def part_1(txt):
    required = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}

    passports = split_clean(txt, "\n\n")
    valids = 0
    for passport in passports:
        matches = TDATA.findall(passport)
        if len(required - set(k for k, v in matches)) < 1:
            valids += 1
    return valids


def part_2(txt):
    required = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}

    passports = split_clean(txt, "\n\n")
    valids = 0
    for passport in passports:
        matches = TDATA.findall(passport)
        if len(required - set(k for k, v in matches)) > 0:
            continue
        valid = True
        for k, v in matches:
            if k == "byr":
                if not (1920 <= int(v) <= 2002):
                    valid = False
                    break
            elif k == "iyr":
                if not (2010 <= int(v) <= 2020):
                    valid = False
                    break
            elif k == "eyr":
                if not (2020 <= int(v) <= 2030):
                    valid = False
                    break
            elif k == "hgt":
                if v[-2:] == "in":
                    if not (59 <= int(v[:-2]) <= 76):
                        valid = False
                        break
                elif v[-2:] == "cm":
                    if not (150 <= int(v[:-2]) <= 193):
                        valid = False
                        break
                else:
                    valid = False
                    break
            elif k == "hcl":
                if v[0] != "#" or not all([c in "0123456789abcdef" for c in v[1:]]):
                    valid = False
                    break
            elif k == "ecl":
                if v not in ("amb", "blu", "brn", "gry", "grn", "hzl", "oth"):
                    valid = False
                    break
            elif k == "pid":
                if len(v) != 9 or not all([c in "0123456789" for c in v[1:]]):
                    valid = False
                    break
        if valid:
            valids += 1
    return valids


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 2)

    def test_part_2(self):
        self.assertEqual(
            part_2(
                """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"""
            ),
            0,
        )
        self.assertEqual(
            part_2(
                """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"""
            ),
            4,
        )


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
