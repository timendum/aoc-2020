import unittest

from utils import load_string, run_testCase, split_clean

TXT_TEST = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"""


def part_1(txt: str, preamble: int) -> int:
    lines = [int(n) for n in split_clean(txt)]
    for n in range(preamble, len(lines)):
        target = lines[n]
        previous = lines[n - preamble : n]
        if target not in [
            previous[i] + previous[j] for i in range(preamble) for j in range(i, preamble)
        ]:
            return target
    return 0


def part_2(txt: str, preamble: int) -> int:
    lines = [int(n) for n in split_clean(txt)]
    for n in range(preamble, len(lines)):
        target = lines[n]
        previous = lines[n - preamble : n]
        if target not in [
            previous[i] + previous[j] for i in range(preamble) for j in range(i, preamble)
        ]:
            break
    for m in range(n):
        for mm in range(m + 2, n):
            s = sum(lines[m:mm])
            if s == target:
                return max(lines[m:mm]) + min(lines[m:mm])
            if s > target:
                break
    return 0


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST, 5), 127)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST, 5), 62)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt, 25))
    print("Part 2: ", part_2(txt, 25))


if __name__ == "__main__":
    main()
