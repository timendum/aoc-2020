import unittest
from typing import Dict

from utils import load_string, run_testCase, split_clean


def part_1(txt: str) -> int:
    lines = [int(n) for n in split_clean(txt)]
    lines = [0] + sorted(lines)
    lines.append(lines[-1] + 3)
    ones = 0
    three = 0
    for i in range(len(lines) - 1):
        if lines[i + 1] - lines[i] == 1:
            ones += 1
        if lines[i + 1] - lines[i] == 3:
            three += 1
    return ones * three


def part_2(txt: str) -> int:
    lines = [int(n) for n in split_clean(txt)]
    lines = [0] + sorted(lines)
    lines.append(lines[-1] + 3)
    known: Dict[int, int] = {}
    known[len(lines)] = 1  # xxxxx
    known[len(lines) - 1] = 1  # (22)
    known[len(lines) - 2] = 1  # 19, (22)

    def subsolution(i: int) -> int:
        if i not in known:
            poss = subsolution(i + 1)
            if lines[i + 2] - lines[i] <= 3:
                poss += subsolution(i + 2)
            if i + 3 < len(lines) and lines[i + 3] - lines[i] <= 3:
                poss += subsolution(i + 3)
            known[i] = poss
        return known[i]

    return subsolution(0)


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1("16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4"), 35)
        self.assertEqual(
            part_1(
                "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38"
                + "\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3"
            ),
            220,
        )

    def test_part_2(self):
        self.assertEqual(part_2("16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4"), 8)
        self.assertEqual(
            part_2(
                "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38"
                + "\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3"
            ),
            19208,
        )


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
