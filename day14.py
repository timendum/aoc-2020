import unittest

from utils import load_string, run_testCase, split_clean


def part_1(txt: str) -> int:
    lines = split_clean(txt)
    bmask = 0
    amask = 0
    mem = {}
    for line in lines:
        cmd, value = line.split(" = ")
        if cmd == "mask":
            bmask = int("".join([c if c == "1" else "0" for c in value]), 2)
            amask = int("".join([c if c == "0" else "1" for c in value]), 2)
        else:
            idx = "".join(list(filter(lambda x: x in "0123456789", cmd)))
            mem[idx] = (int(value) | bmask) & amask
    return sum(mem.values())


def part_2(txt: str) -> int:
    lines = split_clean(txt)
    fixed = 0
    floating = 0
    mem = {}
    for line in lines:
        cmd, value = line.split(" = ")
        if cmd == "mask":
            bmask = int("".join([c if c == "1" else "0" for c in value]), 2)
            amask = int("".join([c if c == "0" else "1" for c in value]), 2)
            fixed = bmask & amask
            floating = (bmask | amask) - fixed
        else:
            idx = int("".join(list(filter(lambda x: x in "0123456789", cmd))))
            idxs = [(int(idx) | fixed | floating) - floating]
            for i in range(36):
                if (1 << i) & floating:
                    new_idxs = []
                    for idx in idxs:
                        new_idxs.append(idx)
                        new_idxs.append(idx + (1 << i))
                    idxs = new_idxs
            for idx in idxs:
                mem[idx] = int(value)
    return sum(mem.values())


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(
            part_1(
                """mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0"""
            ),
            165,
        )

    def test_part_2(self):
        self.assertEqual(
            part_2(
                """mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1"""
            ),
            208,
        )


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
