import unittest
import re
from collections import Counter

from utils import load_string, run_testCase, split_clean

TXT_TEST = """1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc"""

RULE = re.compile(r"(\d+)\-(\d+) (\w): (.+)")


def policy_1(line):
    match = RULE.match(line)
    if not match:
        raise ValueError(line)
    nmin, nmax = [int(x) for x in match.group(1, 2)]
    c = match.group(3)
    txt = match.group(4)
    count = Counter(txt)
    if c not in count:
        return None
    return nmin <= count[c] <= nmax


def part_1(txt):
    return sum([1 for line in split_clean(txt) if policy_1(line)])


def policy_2(line):
    match = RULE.match(line)
    if not match:
        raise ValueError(line)
    nmin, nmax = [int(x) for x in match.group(1, 2)]
    c = match.group(3)
    txt = match.group(4)
    return (txt[nmin - 1] == c) != (txt[nmax - 1] == c)


def part_2(txt):
    return sum([1 for line in split_clean(txt) if policy_2(line)])


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 2)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 1)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
