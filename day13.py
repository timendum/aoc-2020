import unittest

from utils import load_string, run_testCase, split_clean

TXT_TEST = """939
7,13,x,x,59,x,31,19"""


def part_1(txt: str) -> int:
    lines = split_clean(txt)
    earliest = int(lines[0])
    IDs = [int(x) for x in lines[1].split(",") if x != "x"]
    waits = [(i - (earliest % i), i) for i in IDs]
    solution = sorted(waits, key=lambda x: x[0])[0]
    return solution[0] * solution[1]


def part_2(txt: str) -> int:
    lines = split_clean(txt)
    IDs = [int(x) if x != "x" else 1 for x in lines[1].split(",")]
    bbs = [(a, b) for a, b in enumerate(IDs)]
    delta = bbs[0][1]
    x = delta
    for d, n in bbs[1:]:
        while (x + d) % n != 0:
            x += delta
        delta = delta * n
    return x


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 295)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 1068781)
        self.assertEqual(part_2("\n17,x,13,19"), 3417)
        self.assertEqual(part_2("\n67,7,59,61"), 754018)
        self.assertEqual(part_2("\n67,x,7,59,61"), 779210)
        self.assertEqual(part_2("\n67,7,x,59,61"), 1261476)
        self.assertEqual(part_2("\n1789,37,47,1889"), 1202161486)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
