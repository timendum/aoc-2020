import unittest
from typing import Set

from utils import load_string, run_testCase, split_clean

TXT_TEST = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""


def part_1(txt: str) -> int:
    lines = split_clean(txt)
    lineno = 0
    accumulator = 0
    seen: Set[int] = set()
    while True:
        if lineno in seen:
            break
        seen.add(lineno)
        ops, val = lines[lineno].split(" ")
        if ops == "nop":
            lineno += 1
        elif ops == "acc":
            lineno += 1
            accumulator += int(val)
        elif ops == "jmp":
            lineno += int(val)
    return accumulator


def part_2(txt: str) -> int:
    lines = split_clean(txt)
    for changed in range(len(lines)):
        if lines[changed][:3] == "acc":
            continue
        changes = split_clean(txt)
        if changes[changed][:3] == "nop":
            changes[changed] = "jmp" + changes[changed][3:]
        else:
            changes[changed] = "nop" + changes[changed][3:]
        print(changed, changes[changed])
        ok = True
        lineno = 0
        accumulator = 0
        seen: Set[int] = set()
        while lineno != len(lines):
            if lineno in seen:
                ok = False
                break
            seen.add(lineno)
            ops, val = changes[lineno].split(" ")
            if ops == "nop":
                lineno += 1
            elif ops == "acc":
                lineno += 1
                accumulator += int(val)
            elif ops == "jmp":
                lineno += int(val)
        if ok:
            return accumulator
    return 0


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 5)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 8)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
