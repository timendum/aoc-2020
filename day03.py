import unittest

from utils import load_string, run_testCase, split_clean

TXT_TEST = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""


def count_tree(txt, dx, dy):
    grid = split_clean(txt)
    lrow = len(grid[0])
    x, y = (0, 0)
    trees = 0
    while y < len(grid) - dy:
        x = (x + dx) % lrow
        y += dy
        if grid[y][x] == "#":
            trees += 1
    return trees


def part_1(txt):
    return count_tree(txt, 3, 1)


def part_2(txt):
    return (
        count_tree(txt, 1, 1)
        * count_tree(txt, 3, 1)
        * count_tree(txt, 5, 1)
        * count_tree(txt, 7, 1)
        * count_tree(txt, 1, 2)
    )


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 7)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 336)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
