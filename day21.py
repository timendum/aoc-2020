import unittest
from collections import deque
from itertools import islice
from typing import Deque, Set, Tuple

from utils import load_string, run_testCase, split_clean

TXT_TEXT = """Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10"""


def parse(txt: str) -> Tuple[Deque[int], Deque[int]]:
    lines = split_clean(txt)
    separator = lines.index("")
    player1 = deque([int(i) for i in lines[1:separator]])
    player2 = deque([int(i) for i in lines[separator + 2 :]])
    return player1, player2


def part_1(txt: str) -> int:
    player1, player2 = parse(txt)
    while player1 and player2:
        cart1 = player1.popleft()
        cart2 = player2.popleft()
        if cart1 > cart2:
            player1.append(cart1)
            player1.append(cart2)
        elif cart2 > cart1:
            player2.append(cart2)
            player2.append(cart1)
        else:
            raise ValueError("equals!")
    player = player1 or player2
    return sum([(i + 1) * n for i, n in enumerate(reversed(player))])


def play_2(player1: Deque[int], player2: Deque[int]) -> Tuple[int, Deque[int]]:
    seen = set()
    while player1 and player2:
        players = (tuple(player1), tuple(player2))
        if players in seen:
            return 1, player1
        seen.add(players)
        cart1 = player1.popleft()
        cart2 = player2.popleft()
        if len(player1) >= cart1 and len(player2) >= cart2:
            inner1 = deque(islice(player1, cart1))
            inner2 = deque(islice(player2, cart2))
            winner, _ = play_2(inner1, inner2)
        elif cart1 > cart2:
            winner = 1
        elif cart2 > cart1:
            winner = 2
        else:
            raise ValueError("equals!")
        if winner == 1:
            player1.append(cart1)
            player1.append(cart2)
        else:
            player2.append(cart2)
            player2.append(cart1)
    if not player1:
        return 2, player2
    return 1, player1


def part_2(txt: str) -> int:
    player1, player2 = parse(txt)
    _, player = play_2(player1, player2)
    return sum([(i + 1) * n for i, n in enumerate(reversed(player))])


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEXT), 306)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEXT), 291)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
