import unittest
from collections import defaultdict

from utils import load_string, run_testCase, split_clean


def part_1(txt: str) -> int:
    numbers = [int(s) for s in split_clean(txt, ",")]
    for _ in range(len(numbers), 2020):
        n = numbers[-1]
        try:
            idx = numbers[-2::-1].index(n) + 1
            numbers.append(idx)
        except ValueError:
            numbers.append(0)

    return numbers[-1]


def part_2(txt: str) -> int:
    numbers = [int(s) for s in split_clean(txt, ",")]
    idxs = defaultdict(list)
    for i in range(len(numbers)):
        idxs[numbers[i]].append(i)

    n = numbers[-1]
    for i in range(len(numbers), 30000000):
        if len(idxs[n]) < 2:
            n = 0
        else:
            n = i - idxs[n][-2] - 1
        idxs[n].append(i)

    return n


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1("0,3,6"), 436)
        self.assertEqual(part_1("1,3,2"), 1)
        self.assertEqual(part_1("2,1,3"), 10)
        self.assertEqual(part_1("1,2,3"), 27)
        self.assertEqual(part_1("2,3,1"), 78)
        self.assertEqual(part_1("3,2,1"), 438)
        self.assertEqual(part_1("3,1,2"), 1836)

    def test_part_2(self):
        self.assertEqual(part_2("0,3,6"), 175594)
        self.assertEqual(part_2("1,3,2"), 2578)
        self.assertEqual(part_2("2,1,3"), 3544142)
        self.assertEqual(part_2("1,2,3"), 261214)
        self.assertEqual(part_2("2,3,1"), 6895259)
        self.assertEqual(part_2("3,2,1"), 18)
        self.assertEqual(part_2("3,1,2"), 362)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
