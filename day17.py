import unittest
from collections import namedtuple
from itertools import product
from typing import Iterator, Set, Any


from utils import load_string, run_testCase, split_clean


TXT_TEXT = """.#.
..#
###"""


class Point(namedtuple("Point", ["x", "y", "z"])):
    def neighbors(p) -> Iterator["Point"]:
        for dx, dy, dz in product((-1, 0, 1), (-1, 0, 1), (-1, 0, 1)):
            if dx == dy == dz == 0:
                continue
            yield Point(p.x + dx, p.y + dy, p.z + dz)


class PointQ(namedtuple("Point", ["x", "y", "z", "w"])):
    def neighbors(p) -> Iterator["PointQ"]:
        for dx, dy, dz, dw in product((-1, 0, 1), (-1, 0, 1), (-1, 0, 1), (-1, 0, 1)):
            if dx == dy == dz == dw == 0:
                continue
            yield PointQ(p.x + dx, p.y + dy, p.z + dz, p.w + dw)


def solve(txt: str, point_class) -> int:
    lines = split_clean(txt)
    points = set()
    for y, line in enumerate(lines):
        for x, v in enumerate(line):
            if v == "#":
                points.add(point_class(*([x, y] + [0] * (len(point_class._fields) - 2))))
    for _ in range(6):
        new_points = set()
        skipped: Set[Any] = set()
        for point in points:
            if 2 <= len([n for n in point.neighbors() if n in points]) <= 3:
                new_points.add(point)
            for np in point.neighbors():
                if np in skipped:
                    # already checked
                    continue
                if np in points:
                    # already active
                    continue
                if len([n for n in np.neighbors() if n in points]) == 3:
                    new_points.add(np)
                else:
                    skipped.add(np)
        points = new_points
    return len(points)


def part_1(txt: str) -> int:
    return solve(txt, Point)


def part_2(txt: str) -> int:
    return solve(txt, PointQ)


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEXT), 112)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEXT), 848)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
