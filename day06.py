import unittest

from utils import load_string, run_testCase

TXT_TEST = "abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb"


def part_1(txt: str) -> int:
    groups = txt.split("\n\n")
    sn = set("\n")
    return sum([len(set(group) - sn) for group in groups])


def part_2(txt: str) -> int:
    groups = txt.split("\n\n")
    sn = set("\n")
    c = 0
    for group in groups:
        questions = set(group) - sn
        for question in questions:
            if all([question in a for a in group.split("\n")]):
                c += 1
    return c


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEST), 11)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEST), 6)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt))


if __name__ == "__main__":
    main()
