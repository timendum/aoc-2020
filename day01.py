import unittest

from utils import load_string, run_testCase, split_clean

TXT_TEST = """1721
979
366
299
675
1456"""


def find_2_2020(txt):
    numbers = [int(t) for t in split_clean(txt)]
    for i, e in enumerate(numbers):
        for f in numbers[i + 1 :]:
            if e + f == 2020:
                return e * f
    return None


def find_3_2020(txt):
    numbers = [int(t) for t in split_clean(txt)]
    for i, e in enumerate(numbers):
        for j, f in enumerate(numbers[i + 1 :]):
            for g in numbers[i + j + 1 :]:
                if e + f + g == 2020:
                    return e * f * g
    return None


class TestDay(unittest.TestCase):
    def test_find_2_2020(self):
        self.assertEqual(find_2_2020(TXT_TEST), 514579)

    def test_find_3_2020(self):
        self.assertEqual(find_3_2020(TXT_TEST), 241861950)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", find_2_2020(txt))
    print("Part 2: ", find_3_2020(txt))


if __name__ == "__main__":
    main()
