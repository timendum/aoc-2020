import unittest
from typing import Dict, Set

from utils import load_string, run_testCase, split_clean


TXT_TEXT_1 = """class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12"""

TXT_TEXT_2 = """class: 0-1 or 4-19
row: 0-5 or 8-19
seat: 0-13 or 16-19

your ticket:
11,12,13

nearby tickets:
3,9,18
15,1,5
5,14,9"""


class Rules:
    def __init__(self):
        self.rules = {}

    def append(self, line: str) -> None:
        splitted = line.split(": ")
        if len(splitted) != 2 or not all(splitted):
            raise ValueError("Invalid line: " + line)
        self.rules[splitted[0]] = [int(v) for c in splitted[1].split(" or ") for v in c.split("-")]

    def any(self, n: int) -> bool:
        for ranges in self.rules.values():
            if ranges[0] <= n <= ranges[1] or ranges[2] <= n <= ranges[3]:
                return True
        return False

    def valid(self, k: str, n: int) -> bool:
        ranges = self.rules[k]
        return ranges[0] <= n <= ranges[1] or ranges[2] <= n <= ranges[3]


def part_1(txt: str) -> int:
    lines = iter(split_clean(txt))
    rules = Rules()
    for line in lines:
        if not line:
            break
        rules.append(line)
    for line in lines:
        if line == "nearby tickets:":
            break
    invalid = 0
    for line in lines:
        numbers = [int(x) for x in line.split(",")]
        for n in numbers:
            if not rules.any(n):
                invalid += n
    return invalid


def part_2(txt: str, sums: int) -> int:
    lines = iter(split_clean(txt))
    rules = Rules()
    for line in lines:
        if not line:
            break
        rules.append(line)
    if next(lines) != "your ticket:":
        raise ValueError("Expecting your ticket")
    your = [int(x) for x in next(lines).split(",")]
    if next(lines):
        raise ValueError("Expecting blank line")
    if next(lines) != "nearby tickets:":
        raise ValueError("Expecting nearby ticket")
    valids = []
    for line in lines:
        numbers = [int(x) for x in line.split(",")]
        for n in numbers:
            if not rules.any(n):
                break
        else:
            valids.append(numbers)
    poss: Dict[str, Set[int]] = {}
    for k in rules.rules:
        poss[k] = set()
        for i in range(len(your)):
            for numbers in valids:
                if not rules.valid(k, numbers[i]):
                    break
            else:
                poss[k].add(i)
    solutions = [-1] * len(your)
    todo_key = set(poss)
    todo_idx = set(range(len(poss)))
    while todo_idx:
        found_key = None
        found_idx = None
        for key in todo_key:
            if len(poss[key]) == 1:
                found_key = key
                found_idx = next(iter(poss[key]))  # next(iter(poss[key])) = poss[key][0]
                break
        else:
            for idx in todo_idx:
                counts = [k for k, v in poss.items() if idx in v]
                if len(counts) == 1:
                    found_idx = idx
                    found_key = counts[0]
                    break
        if found_key is None or found_idx is None:
            print("Loop")
            return -1
        solutions[list(rules.rules.keys()).index(found_key)] = found_idx
        todo_key.remove(found_key)
        todo_idx.remove(found_idx)
        del poss[found_key]
        for v in poss.values():
            v.remove(found_idx)
        if len(todo_idx) == 1:
            break
    s = 1
    for i in range(sums):
        s *= your[solutions[i]]
    return s


class TestDay(unittest.TestCase):
    def test_part_1(self):
        self.assertEqual(part_1(TXT_TEXT_1), 71)

    def test_part_2(self):
        self.assertEqual(part_2(TXT_TEXT_2, 1), 12)


def main():
    run_testCase(TestDay)
    txt = load_string()
    print("Part 1: ", part_1(txt))
    print("Part 2: ", part_2(txt, 6))


if __name__ == "__main__":
    main()
